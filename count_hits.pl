#!/usr/bin/perl
use strict;
#generates coverage statistics on exon and intron regions of gene capture data
#prerequisite steps: 1)run get_lengths.pl assembled contigs
#2) run bedtools to generate a coverage distribution (genomeCoverageBed -d -ibam bamfile.bam -g contigsfile.fasta)
#create a blast database for your ASSEMBLED contigs, blast your template used for bait design (ie, exons) to them

my $file = $ARGV[0];
my $clen = $ARGV[1]; #in file with contigs in the first column and length in the next
my $file2 = $ARGV[2]; #infile that contains three columns: contig, position, and coverage
my $hit;
my %count;
my %hits;
my %lengths;
my %covs;
my @ids;
open IN, "<$clen";
while (<IN>) { #create a hash with contig id as key and length as its value
	chomp;
	my $line = $_;
	my @field = split(/\s+/,$line);
	my ($contig, $length) = ($field[0], $field[1]);
	$lengths{$contig} = $length;
	}
close IN;

open IN, "<$file2";
while (<IN>) {
	chomp;
	my $line = $_;
	my @field = split(/\s+/,$line);
	my ($id, $position, $coverage) = ($field[0], $field[1], $field[2]);
	push (@ids, $id);
	$covs{$id}{$position} = $coverage;
	}
close IN;

open IN, "<$file";
open OUT, ">$file.edit";
while (<IN>) {
	chomp;
	my $line = $_;
	my ($query, $hit, $thing1, $thing2, $thing3, $thing4, $qstart, $qend, $sstart, $send, $thing10, $thing11) = split(/\s+/, $line);
	if (($qstart<$qend)&&($sstart<$send)) {
		print OUT "$line\n";
	}
	elsif (($qstart>$qend)&&($sstart>$send)) {
		print OUT "$query\t$hit\t$thing1\t$thing2\t$thing3\t$thing4\t$qend\t$qstart\t$send\t$sstart\t$thing10\t$thing11\n";
	}
	elsif (($qstart>$qend)&&($sstart<$send)) {
			print OUT "$query\t$hit\t$thing1\t$thing2\t$thing3\t$thing4\t$qend\t$qstart\t$sstart\t$send\t$thing10\t$thing11\n";
			}
	elsif (($qstart<$qend)&&($sstart>$send)) {
			print OUT "$query\t$hit\t$thing1\t$thing2\t$thing3\t$thing4\t$qstart\t$qend\t$send\t$sstart\t$thing10\t$thing11\n";
		}
	}
close OUT;
close IN;
my $banana = 0;
open IN, "<$file.edit";
while (<IN>){ #create a nested hash, with hit (subject) id, query id, subject start and end
	chomp;
		my $line = $_;
		#print "$line\n";
		my @field = split(/\s+/,$line);
        my ($query, $hit, $sstart, $send) = ($field[0], $field[1], $field[8], $field[9]);
		my $divider = "_";
		my $pos1 = index($query, $divider);
		my $pos2 = rindex($query, $divider);
		my $Qid = substr($query, 0, $pos2);
		my $pos3 = rindex($hit, $divider);
		my $Hid = substr($hit, 0, $pos3);
#		print "$Qid\t$Hid\n";
		if ($Qid =~ $Hid) {
			$hits{$hit}{$query}{$sstart}{$send} = 0;
			$banana++;
			#print "$Qid\t$Hid\n";	
			}
		else {
		#	print "$Qid\t$Hid\n";			
			}
	}
print "$banana\n";

my $totalex = 0;
	my $avgEcov;
	my $avgIcov;
foreach my $hit (sort keys %hits){
	my @exons = ();
	my $sum;
	my $count = 0;
	my $totalex = 0;
	my $intron;
	my $hitlength;
	my @coding;
	my @noncoding;
	my $covEx;
	my $covIn;
	my @covE;
	my @covI;
	my $sumEcov;
	my $sumIcov;
	my $position2;
	foreach my $query (sort {$a<=>$b} keys %{$hits{$hit}}) {
		$count++;
		foreach my $sstart (sort keys %{$hits{$hit}{$query}}) {
		foreach my $send (sort keys %{$hits{$hit}{$query}{$sstart}}) {	
			my $exon=$send-$sstart;
			my $totalex+=$exon+1;
#			print "$hit\t$query\t$totalex\n";
			push (@exons, $totalex);
			my $place = $sstart;
			push (@coding, ($sstart .. $send));
	}}}
#	print "@coding\n";
	for (@exons) {
		$sum+=$_;
	}
#	print "@exons\n";
#	print "$hit\t$sum\n";
#	for my $position2 (sort keys %{$covs{$hit}}) {
			foreach my $position (sort {$a<=>$b} keys %{$covs{$hit}}) {
			if ($position ~~ @coding) {
				$covEx = $covs{$hit}{$position};
				push (@covE, $covEx);
			}
			else {
				$covIn = $covs{$hit}{$position};
				push (@covI, $covIn);
				}
			}
#			print "@covE\n";
#	print "$hit\t@covE\n";
	if (@covE) {
		foreach my $covEx (@covE) {
			$sumEcov+=$covEx;
			}
#			print "$sumEcov\t";
			$avgEcov = $sumEcov/@covE;
#			print "$avgEcov\t";
		}
		
	if (@covI) {
		foreach my $covIn (@covI) {
			$sumIcov+=$covIn;
			}
			$avgIcov = $sumIcov/@covI;
#			print "$avgIcov\n";
		}
		
#	my $avgEcov = $sumEcov/($#covE+1);
#	my $avgIcov = $sumIcov/($#covI+1);
	my $roundE = sprintf("%.2f", $avgEcov);
	my $roundI = sprintf("%.2f", $avgIcov);
	my $hitlength = $lengths{$hit};
	my $intron = $hitlength-$sum;
	print "$hit\t$count\t$hitlength\t$sum\t$intron\t$roundE\t$roundI\n";
	}
