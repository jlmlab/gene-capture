#!/usr/bin/perl
use strict;
use Bio::SeqIO;
#script which will take a multifasta file and generate a two-column output
#with contig/seq name in the first and length in bp in the second

my $file = $ARGV[0];
my $seqfile = Bio::SeqIO -> new (-file => "$file", -format => "fasta");
while (my $io_obj = $seqfile -> next_seq() ) {
	my $header = $io_obj->id();
	my $seq = $io_obj->seq;
	my $length = length($seq);
	print "$header\t$length\n";
}

